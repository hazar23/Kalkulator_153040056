/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import kalkulator_153040056.Kalkulator_153040056;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SB601-26
 */
public class Testing_Kalkulator {
    
    public Testing_Kalkulator() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
     Kalkulator_153040056 kalkulator;
    
    @Before
    public void init(){
        kalkulator=new Kalkulator_153040056();
    }
 
    @Test
    public void testKali() {
        assertEquals(20, kalkulator.kali(10, 2),20);
    }
 
    @Test
    public void testBagi() {
        assertEquals(5, kalkulator.bagi(10, 2),5);
    }
 
    @Test
    public void testTambah() {
        assertEquals(12, kalkulator.tambah(10,2),12);
    }
 
    @Test
    public void testKurang() {
        assertEquals(8, kalkulator.kurang(10, 2),8);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
